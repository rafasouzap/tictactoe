//
//  Played.swift
//  TicTacToe
//
//  Created by Rafael de Paula on 20/08/17.
//  Copyright © 2017 Rafael de Paula. All rights reserved.
//

struct Played {
    
    let player: Player
    let playedType: PlayedType
    let position: QuadrantPosition
    
    enum PlayedType {
        case x
        case o
        
        init(player: Player) {
            switch player.playerType {
            case .human: self = .x
            case .robot: self = .o
            }
        }
    }
    
    init(player: Player, position: Int) {
        self.player = player
        self.playedType = PlayedType(player: player)
        self.position = QuadrantPosition(position: position, playedType: self.playedType)
    }
}
