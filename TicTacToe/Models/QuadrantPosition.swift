//
//  QuadrantPosition.swift
//  TicTacToe
//
//  Created by Rafael de Paula on 20/08/17.
//  Copyright © 2017 Rafael de Paula. All rights reserved.
//

struct QuadrantPosition: Equatable {
    
    let position: Int
    let playedType: Played.PlayedType
    
    static func == (lhs: QuadrantPosition, rhs: QuadrantPosition) -> Bool {
        return lhs == rhs
    }
    
    func check(playerType: Player.PlayerType) -> Bool {
        switch playerType {
        case .human:
            if playedType == .x { return true }
            else { return false }
        case .robot:
            if playedType == .o { return true }
            else { return false }
        }
    }
}

