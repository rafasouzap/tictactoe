//
//  GameActions.swift
//  TicTacToe
//
//  Created by Rafael de Paula on 20/08/17.
//  Copyright © 2017 Rafael de Paula. All rights reserved.
//

struct GameActions {
    
    private var players = [Player]()
    private var actualPlayer: Player?
    private var gameState = GameState.progress
    private var lastPlayed: Played?
    var quadrant: Quadrant = Quadrant()
    var delegate: Gaming?
    
    enum GameState {
        case progress
        case finished(finishedState: FinishedState)
        
        enum FinishedState {
            case win(player: Player)
            case draw
        }
    }
    
    mutating func start() {
        players.append(Player(name: "Humano", playerType: Player.PlayerType.human))
        players.append(Player(name: "Robô", playerType: Player.PlayerType.robot))
        
        actualPlayer = players.first
    }
    
    mutating func restart() {
        gameState = GameState.progress
        quadrant.cleanPositions()
        
        actualPlayer = players.first
    }
    
    
    // MARK: Private funcs
    
    private mutating func checkGameOver(player: Player) -> Bool {
        
        let playedType = Played.PlayedType(player: player)
        var gameOver = Bool()
        
        for positions in Quadrant.positionsToWin {
            for (index, position) in positions.enumerated() {
                if(!quadrant.checkPositions(position: QuadrantPosition(position: position, playedType: playedType))) {
                    break
                }
                else if (index == (positions.count - 1)) {
                    gameOver = true
                }
            }
            if gameOver { break }
        }
        
        if gameOver {
            setGameState(gameState: GameState.finished(finishedState: .win(player: player)))
            return true
        }
        else if quadrant.getPositions().count == Quadrant.maxPositions {
            setGameState(gameState: GameState.finished(finishedState: .draw))
            return true
        }
        return false
    }
    
    private func getNextPlayer(previousPlayer: Player) -> Player {
        
        for player in players {
            if player.playerType != previousPlayer.playerType {
                return player
            }
        }
        assert(false, "getNextPlayer(nenhum player disponível)")
    }
    
    private mutating func setGameState(gameState: GameState) {
        
        self.gameState = gameState
        
        guard let delegate = delegate else {
            print("game delegate did not setted")
            return
        }
        
        switch self.gameState {
        case .finished(finishedState: .win(let winner)):
            for (index, player) in players.enumerated() where player.playerType == winner.playerType {
                players[index].won()
            }
            delegate.finishedGame(state: .win(player: winner))
        case .finished(finishedState: .draw):
            delegate.finishedGame(state: .draw)
        default:
            break
        }
    }
    
    
    // MARK: Game actions funcs
    
    mutating func play(position: Int? = nil, played: Played? = nil) -> Bool {
        
        if let position = position {
            
            guard quadrant.checkFieldPositions(played: Played(player: actualPlayer!, position: position)) else {
                return false
            }
            
            lastPlayed = Played(player: actualPlayer!, position: position)
            
            guard !checkGameOver(player: lastPlayed!.player) else {
                return true
            }
        }
        else if let played = played {
            
            guard quadrant.checkFieldPositions(played: played) else {
                print("Jogada inválida (Robot)")
                return false
            }
            
            guard !checkGameOver(player: played.player) else {
                return true
            }
        }
        
        actualPlayer = getNextPlayer(previousPlayer: actualPlayer!)
        
        guard let actualPlayer = actualPlayer else { return false }
        
        if let robot = players.last, robot.playerType == Player.PlayerType.robot, robot.playerType == actualPlayer.playerType {
            guard let played = robot.doPlay(at: quadrant) else { return false }
            delegate?.robotPlayed(played: played)
        }
        return true
    }
    
    func getLastPlayedType() -> Played.PlayedType? {
        guard let playedType = lastPlayed?.playedType else { return nil }
        return playedType
    }
    
    func getPlayers() -> [Player] {
        return players
    }
}
