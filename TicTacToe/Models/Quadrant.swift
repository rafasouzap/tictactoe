//
//  Quadrant.swift
//  TicTacToe
//
//  Created by Rafael de Paula on 20/08/17.
//  Copyright © 2017 Rafael de Paula. All rights reserved.
//

struct Quadrant {
    
    private var positions = [QuadrantPosition]()
    static let maxPositions = 9
    static let positionsToWin = [[0,1,2],
                                 [3,4,5],
                                 [6,7,8],
                                 [0,3,6],
                                 [1,4,7],
                                 [2,5,8],
                                 [0,4,8],
                                 [2,4,6]]
    
    func getPositions() -> [QuadrantPosition] {
        return positions
    }
    
    mutating func cleanPositions() {
        self.positions = [QuadrantPosition]()
    }
    
    mutating func checkFieldPositions(played: Played) -> Bool {
        guard checkPositionEmpty(position: played.position.position) else {
            print("Posição não está vazia")
            return false
        }
        markFieldPosition(played: played)
        return true
    }
    
    func check(position: Int) -> Bool {
        guard checkPositionEmpty(position: position) else {
            return false
        }
        return true
    }
    
    func check(position: Int, wasPlayedBy: Player) -> Bool {
        return !positions.filter {
            $0.position == position &&
                $0.check(playerType: wasPlayedBy.playerType)
            }.isEmpty
    }
    
    private func checkPositionEmpty(position: Int) -> Bool {
        return positions.filter { $0.position == position }.isEmpty
    }
    
    func checkPositions(position: QuadrantPosition) -> Bool {
        return !positions.filter {
            $0.position == position.position &&
                $0.playedType == position.playedType
            }.isEmpty
    }
    
    private mutating func markFieldPosition(played: Played) {
        self.positions.append(QuadrantPosition(position: played.position.position, playedType: played.playedType))
    }

}
