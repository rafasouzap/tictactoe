//
//  Player.swift
//  TicTacToe
//
//  Created by Rafael de Paula on 20/08/17.
//  Copyright © 2017 Rafael de Paula. All rights reserved.
//

import GameplayKit

struct Player: Playing {
    
    let name: String
    let playerType: PlayerType
    
    var wins = Int()
    
    enum PlayerType {
        case human
        case robot
    }
    
    init(name: String, playerType: PlayerType) {
        self.name = name
        self.playerType = playerType
    }
    
    
    // MARK: Private funcs
    
    mutating func won() {
        self.wins += 1
    }
    
    
    // MARK: Player funcs
    
    func randomPlay(quadrant: Quadrant) -> Played? {
        
        var played: Played?
        var randomPositions = [Int]()
        
        for index in (0 ..< Quadrant.maxPositions)  {
            randomPositions.append(index)
        }
        
        randomPositions = GKRandomSource.sharedRandom().arrayByShufflingObjects(in: randomPositions) as! [Int]
        
        for position in randomPositions {
            if quadrant.check(position: position) {
                played = Played(player: self, position: position)
            }
        }
        return played
    }
    
    func tryPlay(quadrant: Quadrant) -> Played? {
        
        var randomPositions = Quadrant.positionsToWin
        randomPositions = GKRandomSource.sharedRandom().arrayByShufflingObjects(in: randomPositions) as! [[Int]]
        
        // 
        // Tentando fazer uma jogada
        //
        for positions in randomPositions {
            var isOk = true
            var position_ = 99
            
            for position in positions {
                let wasPlayedByMe = quadrant.check(position: position, wasPlayedBy: self)
                let isEmpty = quadrant.check(position: position)
                
                if !isEmpty && !wasPlayedByMe {
                    isOk = false
                    break
                }
                else if isEmpty { position_ = position }
            }
            
            if isOk, position_ != 99 {
                return Played(player: self, position: position_)
            }
        }
        
        return randomPlay(quadrant: quadrant)
    }
    
    func exactPlay(quadrant: Quadrant) -> Played? {
        
        // 
        // Verifica se robô ganha
        //
        for positions in Quadrant.positionsToWin {
            var robotWillWin = Bool()
            var position_ = 99
            var alerts = 0
            
            for position in positions {
                let wasPlayedByMe = quadrant.check(position: position, wasPlayedBy: self)
                let isEmpty = quadrant.check(position: position)
                
                if isEmpty { position_ = position }
                else if wasPlayedByMe { alerts += 1 }
                if alerts >= 2 { robotWillWin = true }
            }
            
            if robotWillWin, position_ != 99 {
                return Played(player: self, position: position_)
            }
        }
        // 
        // Verifica se humano ganha
        //
        for positions in Quadrant.positionsToWin {
            var humanWillWin = Bool()
            var position_ = 99
            var alerts = 0
            
            for position in positions {
                let wasPlayedByMe = quadrant.check(position: position, wasPlayedBy: self)
                let isEmpty = quadrant.check(position: position)
                
                if isEmpty { position_ = position }
                else if !wasPlayedByMe { alerts += 1 }
                if alerts >= 2 { humanWillWin = true }
            }
            
            if humanWillWin, position_ != 99 {
                return Played(player: self, position: position_)
            }
        }
        
        return tryPlay(quadrant: quadrant)
    }
    
    
    // MARK: Protocol
    
    func doPlay(at: Quadrant) -> Played? {
        if self.playerType == .robot { return exactPlay(quadrant: at) }
        else { return nil }
    }
}
