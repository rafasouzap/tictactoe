//
//  Movement.swift
//  TicTacToe
//
//  Created by Rafael de Paula on 20/08/17.
//  Copyright © 2017 Rafael de Paula. All rights reserved.
//

protocol Playing {

    func doPlay(at: Quadrant) -> Played?
    
}
