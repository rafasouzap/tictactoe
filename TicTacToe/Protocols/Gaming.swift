//
//  Gaming.swift
//  TicTacToe
//
//  Created by Rafael de Paula on 20/08/17.
//  Copyright © 2017 Rafael de Paula. All rights reserved.
//

protocol Gaming {
    func finishedGame(state: GameActions.GameState.FinishedState)
    func robotPlayed(played: Played)
}
