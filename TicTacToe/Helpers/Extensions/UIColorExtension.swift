//
//  UIColorExtension.swift
//  TicTacToe
//
//  Created by Rafael de Paula on 19/08/17.
//  Copyright © 2017 Rafael de Paula. All rights reserved.
//

import UIKit

extension UIColor {
    
    class func alphaBlackColor() -> UIColor {
        return UIColor(white: 0, alpha: 0.2)
    }
    
}
