//
//  NSObjectExtension.swift
//  TicTacToe
//
//  Created by Rafael de Paula on 19/08/17.
//  Copyright © 2017 Rafael de Paula. All rights reserved.
//

import UIKit

protocol Identifying { }

extension Identifying where Self : NSObject {
    static var identifier: String {
        return String(describing: self)
    }
}

extension NSObject: Identifying { }
