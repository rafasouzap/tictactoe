//
//  GameBoardFooter.swift
//  TicTacToe
//
//  Created by Rafael de Paula on 19/08/17.
//  Copyright © 2017 Rafael de Paula. All rights reserved.
//

import UIKit

class GameBoardFooter: UICollectionReusableView {
 
    @IBOutlet private weak var numberOfWins: UILabel!
    @IBOutlet private weak var numberOfDraws: UILabel!
 
    func updateWins(player: Player) {
        if player.playerType == Player.PlayerType.human {
            self.numberOfWins.text = "\(player.wins)"
        }
    }
    
    func updateDraws() {
        let drawns: String = numberOfDraws.text!
        numberOfDraws.text = ("\(Int(drawns)! + 1)")
    }
}
