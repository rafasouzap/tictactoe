//
//  GameBoardCell.swift
//  TicTacToe
//
//  Created by Rafael de Paula on 19/08/17.
//  Copyright © 2017 Rafael de Paula. All rights reserved.
//

import UIKit

class GameBoardCell: UICollectionViewCell {
    
    @IBOutlet private weak var quadrant: UILabel!
    
    func setItem(type: Played.PlayedType? = nil) {
        guard let type = type else {
            quadrant.text = ""
            return
        }
        
        switch type {
        case .o: quadrant.text = "O"
        case .x: quadrant.text = "X"
        }
    }
}
