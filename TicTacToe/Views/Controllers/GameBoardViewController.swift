//
//  ViewController.swift
//  TicTacToe
//
//  Created by Rafael de Paula on 16/08/17.
//  Copyright © 2017 Rafael de Paula. All rights reserved.
//

import UIKit

class GameBoardViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, Gaming {

    var gameActions = GameActions()
    var boardHeader = GameBoardHeader()
    var boardFooter = GameBoardFooter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setLayoutSettings()
        
        gameActions.delegate = self
        gameActions.start()
    }
    

    // MARK: Private methods
    
    private func setLayoutSettings() {
        
        let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout
        let screenSize: CGRect = UIScreen.main.bounds
        let cellWidth = screenSize.width / 3
        var cellHeight =  CGFloat()
        
        if let layout = flowLayout, layout.headerReferenceSize.height > 0 {
            cellHeight = (screenSize.height - layout.headerReferenceSize.height - layout.footerReferenceSize.height) / 3
        } else {
            cellHeight = screenSize.height / 3
        }
        flowLayout?.itemSize = CGSize(width: cellWidth, height: cellHeight)
    }
    
    private func showResult(title: String, message: String? = "") {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Começar outra partida", style: .destructive) { _ in
            self.gameActions.restart()
            self.collectionView?.reloadData()
        })
        self.present(alert, animated: true, completion: nil)
    }

    
    // MARK: CollectionView
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
            
        case UICollectionElementKindSectionHeader:
            boardHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                          withReuseIdentifier: GameBoardHeader.identifier,
                                                                          for: indexPath) as! GameBoardHeader
            boardHeader.backgroundColor = UIColor.alphaBlackColor()
            return boardHeader
            
        case UICollectionElementKindSectionFooter:
            boardFooter = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                          withReuseIdentifier: GameBoardFooter.identifier,
                                                                          for: indexPath) as! GameBoardFooter
            boardFooter.backgroundColor = UIColor.alphaBlackColor()
            return boardFooter
            
        default:
            assert(false, "Tipo de elemento inesperado")
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Quadrant.maxPositions
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GameBoardCell.identifier,
                                                      for: indexPath) as! GameBoardCell
        cell.layer.borderColor = UIColor.black.cgColor
        cell.layer.borderWidth = 1
        cell.setItem()
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if gameActions.play(position: indexPath.row) {
            guard let lastPlayedType = gameActions.getLastPlayedType() else { return }
            let cell = collectionView.cellForItem(at: indexPath) as! GameBoardCell
            cell.setItem(type: lastPlayedType)
        }
    }
    
    
    // MARK: Protocol
    
    func finishedGame(state: GameActions.GameState.FinishedState) {
       
        switch state {
        case .win(let winner):
            showResult(title: "\(winner.name) é o vencedor!")
            boardFooter.updateWins(player: winner)
        case .draw:
            showResult(title: "Empate!")
            boardFooter.updateDraws()
        }
    }
    
    func robotPlayed(played: Played) {
        
        let indexPath = IndexPath(row: played.position.position, section: 0)
        if gameActions.play(played: played) {
            let cell = self.collectionView!.cellForItem(at: indexPath) as! GameBoardCell
            cell.setItem(type: played.playedType)
        } else { print("Robot cannot play") }
    }
}
